from os import path

from setuptools import setup, find_packages

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='yaci',
    version='0.1.0',
    packages=find_packages(),
    url='https://gitlab.com/nettnikl/yet-another-capslock-indicator',
    license='GPLv3',
    author='Niklas J. Nett',
    author_email='niklas.j.nett@disroot.org',
    description='A tool to ease the use of caps lock and num lock.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3 :: Only",
        "Environment :: Console",
        "Topic :: Desktop Environment",
        "Operating System :: POSIX :: Linux",
        "Operating System :: Microsoft :: Windows",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    python_requires='>=3.6,<4',
    install_requires=[
        "watchdog>=0.10.2",
        "pynput>=1.6.8",
        "PyQt5>=5.14.0"
    ],
    entry_points={
        'console_scripts': [
            'yaci = yaci.__main__:main'
        ]
    },
    package_data={
        "yaci": ["res/*.ui", "res/themes/*/*.png"],
    }
)
