# Yet Another Capslock Indicator

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![](https://img.shields.io/maintenance/yes/2020)]()

> A tool to ease the use of caps lock and num lock.

A small util that shows a simple notification when pressing lock keys, like caps lock or num lock.
Also, a tray icon displays the current status.  
That way you can easily determine the current status, which is especially useful on a laptop without
keyboard LEDs.

### Compatibility

This tool should be cross-platform compatible.

Currently tested operating systems include:
- Kubuntu 18.04

### Requirements

Python 3 is needed, other needed requirements are listed in `requirements.txt`.
They can be installed using this command.
```bash
pip3 install -r requirements.txt
```  
> If `pip3` is not found, try `python3 -m pip install .` or if you are using windows `py install .`.

pynput is needed to get the key events, PyQt5 is a binding for qt - the used window framework, watchdog
monitors the configuration file for changes.  
On Linux you also have to make sure that `xset` and `grep` are installed.
To do so, use the following command:
```bash
sudo apt install xset grep
```
> If you receive an error like `command not found: apt`, you are probably using non-apt-using distributions, and should
> use the corresponding packaging tool.

### Installation

Under Linux based operation systems, as well as under windows, the installation is very simple.
Open your favorite terminal and execute the following command.
```bash
git clone https://gitlab.com/nettnikl/yet-another-capslock-indicator.git
cd yet-another-capslock-indicator
pip3 install .
```
> If `pip3` is not found, try `python3 -m pip install .` or if you are using windows `py install .`.

> If you want to install the package system-wide on your linux, try instead to run `sudo -H pip3 install .`.

You can now start yaci by typing the following.
```bash
yaci
```

To uninstall, use this command:
```bash
pip3 uninstall yaci
```
or, if yaci was installed system-wide `sudo -H pip3 uninstall yaci`

### Configuration

Use the standalone `config/gui.py` or the "Config" Entry in the tray icon menu to configure this tool.

The settings are stored in the `settings.json` file, which can be found at *~/.yaci*.

### Similar Software

Even if some operating systems provide this functionality out-of-the-box and there are other tools,
this application allows the same experience on different platforms.
And, not to mention, it's free (gratis and open).

- Lenovo Hotkey Features Integration *(Proprietary, Freeware, Windows only)*
- Jonas Kohl's CapsLock Indicator *(Apache 2.0, Windows only)*
- Computer Fix Software Solutions's Caps Lock Commander *(Proprietary, 15 Day Trial, Windows only)*
- stefano-m's CAPS LOCK widget for Awesome *(GPLv3, Awesome only)*
