import os


def is_caps_locked():
    if os.name == 'nt':
        return __is_key_locked_win(0x14)
    else:
        return __is_key_locked_nix(0x01)


def is_num_locked():
    if os.name == 'nt':
        return __is_key_locked_win(0x90)
    else:
        return __is_key_locked_nix(0x02)


def __is_key_locked_nix(key_mask):
    import subprocess

    res = subprocess.check_output('xset q | grep LED', shell=True)
    mask = int(res[58:66])
    return (mask & key_mask) != 0


def __is_key_locked_win(key_id):
    import ctypes

    hll_dll = ctypes.WinDLL("User32.dll")
    key_state = hll_dll.GetKeyState(key_id)
    return (key_state & 0xffff) != 0
