import sys

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from pynput import keyboard

from yaci.caps import is_caps_locked, is_num_locked
from yaci.config import config, change_listener
from yaci.config.gui import ConfigurationWindow
from yaci.notification import CustomWindow
from yaci.theme import Theme


def start_configuration():
    config_window = ConfigurationWindow()
    config_window.show()


class Main(object):
    def __init__(self):
        print('Initialising.')

        self.app = QApplication(sys.argv)
        self.app.setQuitOnLastWindowClosed(False)

        # Load images
        self.theme = Theme(config['themePath'])

        # Create a single instance of the window
        self.window = CustomWindow()

        # Create the tray
        self.tray = QSystemTrayIcon()

        # Create the icon
        if is_caps_locked():
            icon = QIcon(self.theme.pix_caps_lock_on)
        else:
            icon = QIcon(self.theme.pix_caps_lock_off)

        self.tray.setIcon(icon)

        # Create the menu
        menu = QMenu()

        action = QAction("Config")
        action.triggered.connect(start_configuration)
        menu.addAction(action)

        menu.addSeparator()

        action_quit = QAction("Quit")
        action_quit.triggered.connect(self.app.quit)
        menu.addAction(action_quit)

        # Add the menu to the tray
        self.tray.setContextMenu(menu)

        self.tray.setVisible(config['tray'])

        # Create and start a key event listener
        listener = keyboard.Listener(on_press=self.on_key_event, on_release=self.on_key_event)
        listener.start()

        # Monitor for config changes
        change_listener.start(listener=self.refresh_config)

        sys.exit(self.app.exec_())

    def on_key_event(self, key):
        if key == keyboard.Key.caps_lock and config['caps']:
            if is_caps_locked():
                self.set_status(self.theme.pix_caps_lock_on, notify=config['notifyToggleOn'])
            else:
                self.set_status(self.theme.pix_caps_lock_off, notify=config['notifyToggleOff'])
        if key == keyboard.Key.num_lock and config['num']:
            if is_num_locked():
                self.set_status(self.theme.pix_num_lock_on, notify=config['notifyToggleOn'])
            else:
                self.set_status(self.theme.pix_num_lock_off, notify=config['notifyToggleOff'])

    def set_status(self, img, notify=True):
        if notify:
            self.window.img = img
            self.window.setFixedSize(img.size())

            # Move window to the screens center
            screen_geometry = self.app.desktop().screenGeometry()
            x = (screen_geometry.width() - self.window.width()) / 2
            y = (screen_geometry.height() - self.window.height()) / 2
            self.window.move(x, y)

            # Trigger an event in the window
            self.window.caps.emit()

        # Update tray icon
        self.tray.setIcon(QIcon(img))

    def refresh_config(self):
        self.theme = Theme(config['themePath'])

        # Load transparency setting
        self.window.setWindowOpacity(config['notificationTransparency']/100)

        self.tray.setVisible(config['tray'])


def main():
    Main()


if __name__ == '__main__':
    main()
