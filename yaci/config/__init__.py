import json
from os import path, mkdir
from json import JSONDecodeError

SETTINGS_PATH = path.expanduser("~/.yaci")
SETTINGS_FILE = path.join(SETTINGS_PATH, "settings.json")


def ensure_settings_folder():
    if not path.isdir(SETTINGS_PATH):
        mkdir(SETTINGS_PATH)


def load():
    if path.isfile(SETTINGS_FILE):
        with open(SETTINGS_FILE) as infile:
            try:
                config.update(json.load(infile))
            except JSONDecodeError as e:
                print('Invalid file: '+SETTINGS_FILE)
                print(e)


def save():
    with open(SETTINGS_FILE, 'w') as outfile:
        json.dump(config, outfile)


default_settings = {
    "caps": True,
    "num": False,
    "tray": True,
    "notifyToggleOn": True,
    "notifyToggleOff": True,
    "notificationStayTime": 2000,
    "notificationTransparency": 100,
    "themePath": path.join(path.dirname(path.realpath(__file__)), '..', 'res', 'themes', 'theme1')
}

config = default_settings
load()
ensure_settings_folder()
