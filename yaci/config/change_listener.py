from json import JSONDecodeError

from watchdog.events import PatternMatchingEventHandler

from yaci import config
from yaci.config import SETTINGS_FILE, SETTINGS_PATH

from watchdog.observers import Observer


class UpdaterHandler(PatternMatchingEventHandler):

    def __init__(self, listener, patterns=None, ignore_patterns=None, ignore_directories=False, case_sensitive=False):
        super().__init__(patterns, ignore_patterns, ignore_directories, case_sensitive)
        self.listener = listener

    def on_any_event(self, event):
        try:
            print('Loading config.')
            config.load()
            print(config.config)
        except JSONDecodeError as e:
            print(e)

        self.listener()


def start(listener):
    event_handler = UpdaterHandler(listener=listener, patterns=[SETTINGS_FILE], ignore_directories=True)
    observer = Observer()
    observer.schedule(event_handler, SETTINGS_PATH, recursive=True)
    observer.start()
