import sys
from os import path

from PyQt5 import uic
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QApplication, QMainWindow, QShortcut

from yaci import config
from yaci.theme import list_themes


class ConfigurationWindow(QMainWindow):

    def __init__(self, flags=None, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)

        print('Loading ui.')
        uic.loadUi(path.join(path.dirname(path.realpath(__file__)), '..', 'res', 'configurationWindow.ui'), self)

        print('Loading current configuration.')
        self.checkBox_caps.setChecked(config.config["caps"])
        self.checkBox_num.setChecked(config.config["num"])
        self.checkBox_toggle_on.setChecked(config.config["notifyToggleOn"])
        self.checkBox_toggle_off.setChecked(config.config["notifyToggleOff"])
        self.spinBox_stay_time.setValue(config.config["notificationStayTime"])
        self.horizontalSlider.setValue(config.config["notificationTransparency"])
        self.listWidget_themePath.addItems(list_themes())

        self.checkBox_caps.stateChanged.connect(lambda: store_value("caps", self.checkBox_caps.isChecked()))
        self.checkBox_num.stateChanged.connect(lambda: store_value("num", self.checkBox_num.isChecked()))
        self.checkBox_toggle_on.stateChanged.connect(lambda: store_value("notifyToggleOn", self.checkBox_toggle_on.isChecked()))
        self.checkBox_toggle_off.stateChanged.connect(lambda: store_value("notifyToggleOff", self.checkBox_toggle_off.isChecked()))
        self.spinBox_stay_time.valueChanged.connect(lambda: store_value("notificationStayTime", self.spinBox_stay_time.value()))
        self.horizontalSlider.valueChanged.connect(lambda: store_value("notificationTransparency", self.horizontalSlider.value()))
        self.listWidget_themePath.currentItemChanged.connect(lambda: store_value("themePath", self.listWidget_themePath.currentItem().text()))

        QShortcut(QKeySequence('esc'), self, self.close)
        QShortcut(QKeySequence('ctrl+q'), self, self.close)


def store_value(name, value):
    config.config[name] = value
    config.save()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = ConfigurationWindow()
    window.show()
    sys.exit(app.exec_())
