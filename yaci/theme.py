from os import path, scandir

from PyQt5.QtGui import QPixmap

THEME_DIR = path.join(path.dirname(path.realpath(__file__)), 'res', 'themes')


class Theme:
    def __init__(self, theme_path):
        self.pix_caps_lock_off = QPixmap(path.join(theme_path, "caps-lock-off.png"))
        self.pix_caps_lock_on = QPixmap(path.join(theme_path, "caps-lock-on.png"))
        self.pix_num_lock_off = QPixmap(path.join(theme_path, "num-lock-off.png"))
        self.pix_num_lock_on = QPixmap(path.join(theme_path, "num-lock-on.png"))


def list_themes():
    return [f.path for f in scandir(THEME_DIR) if f.is_dir()]
