from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from yaci.config import config


class CustomWindow(QMainWindow):

    # Signal used to communicate the key-event from the key listener-thread to the GUI-thread
    caps = pyqtSignal()

    def __init__(self, flags=None, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)

        self.img = None

        self.setWindowTitle("Yet Another Capslock Indicator")

        # Create a single Timer that hides the window
        self.current_timer = QTimer()
        self.current_timer.timeout.connect(self.hide)
        self.current_timer.setSingleShot(True)

        # Make the window undecorated, transparent, click-through, on-top, not-in-taskbar
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setAttribute(Qt.WA_TransparentForMouseEvents, True)
        self.setAttribute(Qt.WA_X11DoNotAcceptFocus, True)
        self.setAttribute(Qt.WA_ShowWithoutActivating, True)
        self.setAttribute(Qt.WA_AlwaysStackOnTop, True)
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setWindowFlag(Qt.WindowStaysOnTopHint)
        self.setWindowFlag(Qt.Tool)

        self.setWindowOpacity(config['notificationTransparency']/100)

        # Register the caps-event listener
        self.caps.connect(self.caps_event)

    def caps_event(self):
        # Show window if hidden
        self.show()

        # Update window if different image
        self.update()

        # Restart the close-window countdown timer
        self.current_timer.start(config['notificationStayTime'])

    def paintEvent(self, event=None):
        painter = QPainter(self)

        # Draw the image directly to the transparent canvas
        painter.drawPixmap(0, 0, self.img)
